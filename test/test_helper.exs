# Ensure the start of ExMachina factory module
{:ok, _} = Application.ensure_all_started(:ex_machina)

ExUnit.start

Ecto.Adapters.SQL.Sandbox.mode(Backstage.Repo, :manual)
