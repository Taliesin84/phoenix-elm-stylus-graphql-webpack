defmodule Backstage do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(Backstage.Repo, []),
      # Start the endpoint when the application starts
      supervisor(Backstage.Endpoint, []),
      # Start your own worker by calling: Backstage.Worker.start_link(arg1, arg2, arg3)
      # worker(Backstage.Worker, [arg1, arg2, arg3]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Backstage.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Backstage.Endpoint.config_change(changed, removed)
    :ok
  end
end
