defmodule Backstage.Factory do

  use ExMachina.Ecto, repo: Backstage.Repo


  def user_factory do
    first_name = Faker.Name.first_name
    last_name = Faker.Name.last_name
    %Backstage.User{
      first_name: first_name,
      last_name: last_name,
      email: sequence(:email, &"#{first_name}.#{last_name}#{&1}@#{Faker.Internet.free_email_service}"),
      password_hash: "plop"
    }
  end

end
