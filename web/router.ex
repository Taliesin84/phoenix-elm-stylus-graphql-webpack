defmodule Backstage.Router do
  use Backstage.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug Backstage.Context # GraphQL context
  end

  scope "/", Backstage do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/about", AboutController, :index

    resources "/users", UserController
  end

  # Other scopes may use custom stacks.
  scope "/api" do
    pipe_through :api

    get "/graphiql", Absinthe.Plug.GraphiQL,
      schema: Backstage.Schema
    post "/graphiql", Absinthe.Plug.GraphiQL,
      schema: Backstage.Schema
    forward "/", Absinthe.Plug,
      schema: Backstage.Schema
  end
end
