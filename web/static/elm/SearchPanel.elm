module SearchPanel exposing (..)

import Html.App as Html
import Html exposing (..)

pluralize : String -> String -> Int -> String
pluralize singular plural quantity =
  if quantity == 1 then
    singular
  else
    plural

main =
  Html.beginnerProgram
  { view = view
  , update = update
  , model = model
  }

-- model

type alias Model = Int

model : Model
model = 1

view model =
  div []
    [ h1 [] [text "Hello Elm !!!"]
    , h4 [] [text (pluralize "shelf" "shelves" 1)]
    ]

update model msg =
  model
