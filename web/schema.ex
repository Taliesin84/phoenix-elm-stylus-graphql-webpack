defmodule Backstage.Schema do
  use Absinthe.Schema

  alias Backstage.Resolver

  import_types Backstage.Schema.Types

  query do
    field :user, :user do
      arg :id, non_null(:id)
      resolve &Resolver.User.find/3
    end

    field :users, list_of(:user) do
      resolve &Resolver.User.all/3
    end
  end

  mutation do
    @desc "Create a User"
    field :create_user, :user do
      arg :first_name, non_null(:string)
      arg :last_name, non_null(:string)
      arg :email, non_null(:string)
      arg :password_hash, non_null(:string)
      resolve &Resolver.User.create/3
    end
  end
end
