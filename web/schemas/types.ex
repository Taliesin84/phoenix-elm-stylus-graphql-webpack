defmodule Backstage.Schema.Types do
  use Absinthe.Schema.Notation

  @desc """
  A registered user of Backstage
  """
  object :user do
    field :id, :id
    field :first_name, :string
    field :last_name, :string
    field :email, :string
  end

end
