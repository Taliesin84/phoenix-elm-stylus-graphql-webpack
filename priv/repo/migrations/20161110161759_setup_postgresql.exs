defmodule Backstage.Repo.Migrations.SetupPostgresql do
  use Ecto.Migration

  def up do
    execute "CREATE EXTENSION if not exists fuzzystrmatch;"
    execute "CREATE EXTENSION if not exists unaccent;"
    execute "CREATE EXTENSION if not exists pg_trgm;"
  end

  def down do
    execute "DROP EXTENSION fuzzystrmatch;"
    execute "DROP EXTENSION unaccent;"
    execute "DROP EXTENSION pg_trgm;"
  end
end
