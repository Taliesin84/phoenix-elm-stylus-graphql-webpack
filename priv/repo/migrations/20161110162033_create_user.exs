defmodule Backstage.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string, null: false
      add :password_hash, :string, null: false

      add :first_name, :string
      add :last_name, :string
      add :language, :string, default: "en"

      timestamps()
    end

    create unique_index(:users, [:email])

  end
end
