# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Backstage.Repo.insert!(%Backstage.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

import Backstage.Factory

alias Backstage.Repo

# admin_params
# admin = User.changeset(%User{}, user_params)
# Repo.insert!()

build_list(99, :user)
|> Enum.each( &Repo.insert!(&1) )
