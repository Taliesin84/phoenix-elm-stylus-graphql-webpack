use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :backstage, Backstage.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [npm: [ "start", cd: Path.expand("../", __DIR__)] ]
  # watchers: [ node: ["node_modules/brunch/bin/brunch", "watch", "--stdin"] ]


# Watch static and templates for browser reloading.
config :backstage, Backstage.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|elm|css|styl|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex|slim)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :backstage, Backstage.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "taliesin",
  password: "*SilVerthorN1984*",
  database: "backstage_dev",
  hostname: "localhost",
  pool_size: 10
