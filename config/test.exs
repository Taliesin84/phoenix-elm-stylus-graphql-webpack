use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :backstage, Backstage.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :backstage, Backstage.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "taliesin",
  password: "*SilVerthorN1984*",
  database: "backstage_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
