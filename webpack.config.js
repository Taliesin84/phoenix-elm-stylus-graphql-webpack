const webpack = require('webpack')
const path = require('path')

// Webpack plugins
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

// Stylus plugins
const postStylus = require('poststylus')
const rupture = require('rupture')
const nib = require('nib')

// PostCSS plugins
const cssFocus = require('postcss-focus')
const cssMQPaker = require('css-mqpacker')
const lostGrid = require('lost')

// Variables
const root = path.resolve(__dirname)
const isDev = process.env.NODE_ENV !== 'production'
const extractCSS = new ExtractTextPlugin('css/app.css', { allChunks: true })

module.exports = {

  devtool: isDev ? 'inline-sourcemap' : null,

  entry: {
    searchPanelApp: [
      './web/static/css/app.styl',
      './web/static/js/searchPanelApp.js' ]
  },

  output: {
    path: './priv/static',
    filename: 'js/[name].js'
  },

  resolve: {
    moduleDirectories: [ 'node_modules', path.join(__dirname, '/web/static/js') ],
    extensions: ['', '.js', '.elm', '.css', '.styl'],
    alias: {
      phoenix: path.join(__dirname, '../deps/phoenix/web/static/js/phoenix.js')
    }
  },

  plugins: isDev
    ? [
      // new webpack.NoErrorsPlugin(),
      extractCSS ]
    : [
      extractCSS,
      new webpack.NoErrorsPlugin(),
      new CopyWebpackPlugin([{ from: './web/static/assets' }]),
      new webpack.optimize.UglifyJsPlugin({
        compress: { warnings: false },
        comments: false
      }) ],

  module: {
    noParse: /\.elm$/,
    // preLoaders: [
    //   {
    //     test: /\.styl$/,
    //     loader: 'stylint',
    //     exclude: /node_modules/
    //   }, {
    //     test: /\.js$/,
    //     loader: 'eslint',
    //     exclude: [/node_modules/, /elm_stuff/]
    //   }
    // ],
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        include: root
      }, {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        loaders: [
          'elm-hot',
          'elm-webpack?verbose=true&warn=true'
        ]
      }, {
        test: /\.styl$/,
        loader: extractCSS.extract(['css', 'postcss', 'stylus'])
      }, {
        test: /\.css$/,
        loader: extractCSS.extract(['css', 'postcss'])
      }, {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'file',
        query: {
          limit: 10,
          name: 'images/[name].[ext]'
        }
      }, {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'file',
        query: {
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  },

  postcss: () => {
    [ cssFocus(), cssMQPaker(), lostGrid() ]
  },

  stylus: {
    use: [
      postStylus([
        cssFocus(),
        cssMQPaker(),
        lostGrid()
      ]),
      rupture(),
      nib()
    ]
    // import: [
    //   '~/nib/lib/nib/index.styl',
    //   '~/rupture/rupture/index.styl'
    // ]
  },

  devServer: {
    inline: true,
    progress: true,
    stats: { colors: true }
  }

}
